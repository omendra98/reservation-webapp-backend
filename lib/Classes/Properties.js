"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PropertiesClass = void 0;
// import collections
const Properties_1 = __importDefault(require("../Schemas/Properties/Properties"));
let schema = require("../Schemas/connectMongo.js");
class PropertiesClass {
    constructor() {
    }
    /**
     * @desc Function to create new Property
     * @param {Property}} payload
     * @returns {Property}
     */
    async createProperty(payload) {
        console.log({ message: `Process started`, payload });
        try {
            let result = await Properties_1.default.create(payload);
            console.log({ message: `Result from DB`, result });
            return result;
        }
        catch (error) {
            console.log({ message: "Error on database", error });
            throw error;
        }
    }
    /**
     * @desc Function to get all properties for a super admin/host
     * @param {Property}} payload
     * @returns {Property}
     */
    async getProperties(payload) {
        console.log({ message: `Process started`, payload });
        try {
            if (!payload.isActive)
                payload.isActive = true;
            let result = await Properties_1.default.find(payload).sort({ createdAt: -1 });
            console.log({ message: `DB result`, result });
            return result;
        }
        catch (error) {
            console.log({ message: "Error on database", error });
            throw error;
        }
    }
    /**
     * @desc Function to get specific property for a super admin/host
     * @param {Property}} payload
     * @returns {Property}
     */
    async getProperty(payload) {
        console.log({ message: `Process started`, payload });
        try {
            if (!payload.isActive)
                payload.isActive = true;
            let result = await Properties_1.default.findOne(payload).sort({ createdAt: -1 });
            console.log({ message: `DB result`, result });
            return result;
        }
        catch (error) {
            console.log({ message: "Error on database", error });
            throw error;
        }
    }
    /**
     * @desc Function to get update specific property for a super admin/host
     * @param {{ superAdminId : string , propertyId : string, name? : string , timeZone? : string , address? : string , isDisabled? : string }perty}} payload
     * @returns {Property}
     */
    async updateProperty(payload) {
        console.log({ message: `Process started`, payload });
        try {
            let updateData = {};
            let filters = {};
            filters.superAdminId = payload.superAdminId ? payload.superAdminId : {};
            filters._id = payload.propertyId ? payload.propertyId : {};
            if (payload.name)
                updateData.name = payload.name;
            if (payload.timeZone)
                updateData.timeZone = payload.timeZone;
            if (payload.address)
                updateData.address = payload.address;
            if (payload.isDisabled)
                updateData.isDisabled = payload.isDisabled;
            console.log(updateData);
            console.log(filters);
            let result = await Properties_1.default.updateOne(filters, updateData).sort({ createdAt: -1 });
            console.log({ message: `DB result`, result });
            return result ? true : false;
        }
        catch (error) {
            throw error;
        }
    }
    /**
     * @desc Function to get delete specific property for a super admin/host
     * @param {{ superAdminId : string , propertyId : string}}} payload
     * @returns {boolean}
     */
    async deleteProperty(payload) {
        console.log({ message: `Process started`, payload });
        try {
            //check if property is valid or not
            let updateData = {
                isActive: 0
            };
            let filters = {};
            filters.superAdminId = payload.superAdminId ? payload.superAdminId : {};
            filters._id = payload.propertyId ? payload.propertyId : {};
            console.log(filters, updateData);
            let result = await Properties_1.default.updateOne(filters, updateData).sort({ createdAt: -1 });
            console.log({ message: `DB result`, result });
            return result ? true : false;
        }
        catch (error) {
            throw error;
        }
    }
}
exports.PropertiesClass = PropertiesClass;
