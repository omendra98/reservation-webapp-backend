import { Reservation } from "../Datatypes/reservations";
export declare class ReservationClass {
    constructor();
    /**
     * @desc Function to add a stay on property
     * @conditions If stay start time is less than current time,
     * or stay end time is greater than 3 months from now then this fucntions will throw error
     * @param {{ Reservation }} payload
     * @returns {Reservation}
     */
    addStay(payload: Reservation): Promise<any>;
    /**
     * @desc Function to update a stay on property
     * @conditions Hosts(Super Admin & Host type user) can only update the stay to mark it as cancelled
     * @param {{ cancelled : boolean , superAdminId : string, reservationId : string }} payload
     * @returns {boolean}
     */
    updateStayByHosts(payload: {
        cancelled: boolean;
        superAdminId: string;
        reservationId: string;
    }): Promise<boolean>;
    /**
     * @desc Function to update a stay on property by Guests
     * @conditions Guests can only update the stay to mark the stay as checked in or checked out
     * @param {{ checkedIn? : boolean , checkedOut?: boolean, superAdminId : string, reservationId : string } }} payload
     * @returns {boolean}
     */
    updateStayByGuest(payload: {
        checkedIn?: boolean;
        checkedOut?: boolean;
        superAdminId: string;
        reservationId: string;
    }): Promise<boolean>;
    /**
     * @desc Function to get all the reservations for a property
     * @param {{ Reservation }} payload
     * @returns {Reservation}
     */
    getReservationsByProperty(payload: Partial<Reservation>): Promise<any[]>;
    /**
     * @desc Function to get all the current reservations for a property
     * @param {{ Reservation }} payload
     * @returns {Reservation}
     */
    getCurrentReservationsByProperty(payload: {
        propertyId: string;
        superAdminId: string;
    }): Promise<any[]>;
    /**
    * @desc Function to get all the upcoming reservations for a property
    * @param {{ Reservation }} payload
    * @returns {Reservation}
    */
    getUpcomingReservationsByProperty(payload: {
        propertyId: string;
        superAdminId: string;
    }): Promise<any[]>;
    /**
     * @desc Function to get all the reservations associated to a guest
     * @param {{ guestId : string, superAdminId : string }} payload
     * @returns {Reservation}
     */
    getReservationsForGuest(payload: {
        guestId: string;
        superAdminId: string;
    }): Promise<any[]>;
    /**
     * @desc Function to get all the reservations for a super admin or host
     * @param {{ Reservation }} payload
     * @returns {Reservation}
     */
    getAllReservations(payload: Partial<Reservation>): Promise<any[]>;
}
