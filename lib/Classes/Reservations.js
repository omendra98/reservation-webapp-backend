"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReservationClass = void 0;
// import collections
const Reservations_1 = __importDefault(require("../Schemas/Reservations/Reservations"));
const Properties_1 = __importDefault(require("../Schemas/Properties/Properties"));
const Users_1 = __importDefault(require("../Schemas/Users/Users"));
let schema = require("../Schemas/connectMongo.js");
class ReservationClass {
    constructor() {
    }
    /**
     * @desc Function to add a stay on property
     * @conditions If stay start time is less than current time,
     * or stay end time is greater than 3 months from now then this fucntions will throw error
     * @param {{ Reservation }} payload
     * @returns {Reservation}
     */
    async addStay(payload) {
        console.log({ message: `Process started`, payload });
        try {
            //Validate property 
            let property = await Properties_1.default.find({
                _id: payload.propertyId,
                superAdminId: payload.superAdminId
            });
            console.log("@@@@property", property);
            if (property && Object.keys(property).length == 0) {
                throw ("Property id is not valid");
            }
            //Validate Guest 
            let user = await Users_1.default.find({
                _id: payload.guestId,
                type: "Guest",
                superAdminId: payload.superAdminId
            });
            console.log("@@@@user", user);
            if (user && Object.keys(user).length == 0) {
                throw ("Guest id is not valid");
            }
            let input = payload;
            const startDate = payload.startDate;
            const endDate = payload.endDate;
            var currentDate = new Date();
            var futureDate = new Date();
            var parsedStartDate = new Date(startDate);
            var parsedEndDate = new Date(endDate);
            futureDate.setDate(futureDate.getDate() + 90);
            console.log(futureDate);
            if (parsedStartDate < currentDate) {
                throw "Access Start Date is in past";
            }
            if (parsedEndDate > futureDate) {
                throw "Access End Date is after 90 days from now";
            }
            input.startDate = parsedStartDate;
            input.endDate = parsedEndDate;
            console.log(input);
            let result = await Reservations_1.default.create(input);
            console.log({ message: `Result from DB`, result });
            return result;
        }
        catch (error) {
            console.log({ message: "Error on database", error });
            throw error;
        }
    }
    /**
     * @desc Function to update a stay on property
     * @conditions Hosts(Super Admin & Host type user) can only update the stay to mark it as cancelled
     * @param {{ cancelled : boolean , superAdminId : string, reservationId : string }} payload
     * @returns {boolean}
     */
    async updateStayByHosts(payload) {
        console.log({ message: `Process started`, payload });
        try {
            //Validate Stay 
            let stay = await Reservations_1.default.find({
                _id: payload.reservationId,
                superAdminId: payload.superAdminId
            });
            console.log("@@@@stay", stay);
            if (stay && Object.keys(stay).length == 0) {
                throw ("Reservation id is not valid");
            }
            let updateData = {};
            let filters = {};
            filters.superAdminId = payload.superAdminId;
            filters._id = payload.reservationId;
            if (!!payload.cancelled) {
                if (payload.cancelled)
                    updateData.cancelled = payload.cancelled;
            }
            console.log(updateData);
            console.log(filters);
            let result = await Reservations_1.default.updateOne(filters, updateData).sort({ createdAt: -1 });
            console.log({ message: `DB result`, result });
            return true;
        }
        catch (error) {
            console.log({ message: "Error on database", error, payload });
            throw error;
        }
    }
    /**
     * @desc Function to update a stay on property by Guests
     * @conditions Guests can only update the stay to mark the stay as checked in or checked out
     * @param {{ checkedIn? : boolean , checkedOut?: boolean, superAdminId : string, reservationId : string } }} payload
     * @returns {boolean}
     */
    async updateStayByGuest(payload) {
        console.log({ message: `Process started`, payload });
        try {
            let updateData = {};
            let filters = {};
            filters.superAdminId = payload.superAdminId;
            filters._id = payload.reservationId;
            if (!payload.checkedIn && !payload.checkedOut) {
                throw "Check in & Check out status is not passed";
            }
            if (!!payload.checkedIn || !!payload.checkedOut) {
                if (payload.checkedOut)
                    updateData.checkedOut = payload.checkedOut;
                if (payload.checkedIn)
                    updateData.checkedIn = payload.checkedIn;
            }
            console.log(updateData);
            console.log(filters);
            let result = await Reservations_1.default.updateOne(filters, updateData).sort({ createdAt: -1 });
            console.log({ message: `DB result`, result });
            return true;
        }
        catch (error) {
            console.log({ message: "Error on database", error, payload });
            throw error;
        }
    }
    /**
     * @desc Function to get all the reservations for a property
     * @param {{ Reservation }} payload
     * @returns {Reservation}
     */
    async getReservationsByProperty(payload) {
        console.log({ message: `Process started`, payload });
        try {
            if (!payload.cancelled)
                payload.cancelled = false;
            let result = await Reservations_1.default.find(payload).sort({ createdAt: -1 });
            console.log({ message: `DB result`, result });
            return result;
        }
        catch (error) {
            console.log({ message: "Error on database", error, payload });
            throw error;
        }
    }
    /**
     * @desc Function to get all the current reservations for a property
     * @param {{ Reservation }} payload
     * @returns {Reservation}
     */
    async getCurrentReservationsByProperty(payload) {
        console.log({ message: `Process started`, payload });
        try {
            let input = payload;
            var currentDate = new Date();
            input.cancelled = false;
            let result = await Reservations_1.default.find({
                propertyId: payload.propertyId,
                startDate: { $lt: currentDate }
            }).sort({ createdAt: -1 });
            console.log({ message: `DB result`, result });
            return result;
        }
        catch (error) {
            console.log({ message: "Error on database", error, payload });
            throw error;
        }
    }
    /**
    * @desc Function to get all the upcoming reservations for a property
    * @param {{ Reservation }} payload
    * @returns {Reservation}
    */
    async getUpcomingReservationsByProperty(payload) {
        console.log({ message: `Process started`, payload });
        try {
            let input = payload;
            var currentDate = new Date();
            input.cancelled = false;
            let result = await Reservations_1.default.find({
                propertyId: payload.propertyId,
                startDate: { $gt: currentDate }
            }).sort({ createdAt: -1 });
            console.log({ message: `DB result`, result });
            return result;
        }
        catch (error) {
            console.log({ message: "Error on database", error, payload });
            throw error;
        }
    }
    /**
     * @desc Function to get all the reservations associated to a guest
     * @param {{ guestId : string, superAdminId : string }} payload
     * @returns {Reservation}
     */
    async getReservationsForGuest(payload) {
        console.log({ message: `Process started`, payload });
        try {
            let result = await Reservations_1.default.find(payload).sort({ createdAt: -1 });
            console.log({ message: `DB result`, result });
            return result;
        }
        catch (error) {
            console.log({ message: "Error on database", error, payload });
            throw error;
        }
    }
    /**
     * @desc Function to get all the reservations for a super admin or host
     * @param {{ Reservation }} payload
     * @returns {Reservation}
     */
    async getAllReservations(payload) {
        console.log({ message: `Process started`, payload });
        try {
            if (!payload.cancelled)
                payload.cancelled = false;
            let result = await Reservations_1.default.find(payload).sort({ createdAt: -1 });
            console.log({ message: `DB result`, result });
            return result;
        }
        catch (error) {
            console.log({ message: "Error on database", error, payload });
            throw error;
        }
    }
}
exports.ReservationClass = ReservationClass;
// (()=>{
//     var test = new ReservationClass()
//     var input2 = { 
//         propertyId: "64fef1a10dc65314d8cbd014",
//         superAdminId : "64fec586cfcc252d58c99ec7"
//     }
//     test.getUpcomingReservationsByProperty(input2);
// })()
