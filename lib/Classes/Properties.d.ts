import { Property } from "../Datatypes/properties";
export declare class PropertiesClass {
    constructor();
    /**
     * @desc Function to create new Property
     * @param {Property}} payload
     * @returns {Property}
     */
    createProperty(payload: Property): Promise<any>;
    /**
     * @desc Function to get all properties for a super admin/host
     * @param {Property}} payload
     * @returns {Property}
     */
    getProperties(payload: Partial<Property>): Promise<any[]>;
    /**
     * @desc Function to get specific property for a super admin/host
     * @param {Property}} payload
     * @returns {Property}
     */
    getProperty(payload: Partial<Property>): Promise<any>;
    /**
     * @desc Function to get update specific property for a super admin/host
     * @param {{ superAdminId : string , propertyId : string, name? : string , timeZone? : string , address? : string , isDisabled? : string }perty}} payload
     * @returns {Property}
     */
    updateProperty(payload: {
        superAdminId: string;
        propertyId: string;
        name?: string;
        timeZone?: string;
        address?: string;
        isDisabled?: string;
    }): Promise<boolean>;
    /**
     * @desc Function to get delete specific property for a super admin/host
     * @param {{ superAdminId : string , propertyId : string}}} payload
     * @returns {boolean}
     */
    deleteProperty(payload: {
        superAdminId: string;
        propertyId: string;
    }): Promise<boolean>;
}
