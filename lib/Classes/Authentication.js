"use strict";
//import constants
//import third party modules
//import crypto from 'crypto';
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Authentication = void 0;
//import modules
const Users_1 = __importDefault(require("../Schemas/Users/Users"));
const ObjectParser_1 = require("./../Helper/ObjectParser");
let schema = require("../Schemas/connectMongo.js");
var bcrypt = require('bcryptjs');
var crypto = require('crypto');
//local modules
const AuthenticationHelper_1 = require("./../Helper/AuthenticationHelper");
class Authentication {
    constructor() {
        this.objectParser = new ObjectParser_1.ObjectParser();
        this.authenticationHelper = new AuthenticationHelper_1.AuthenticationHelper();
    }
    /**
     * @desc Function to create new Super Admin
     * @param {signUpRequest}} payload
     * @returns {userResponse}
     */
    async signUp(payload) {
        console.log("started with payload", payload);
        try {
            if (payload.type != "SuperAdmin") {
                throw "User type is not correct";
            }
            //get user 
            let user = await Users_1.default.find({
                email: payload.email,
                deleted: 0
            });
            console.log("@@@@user", user);
            if (user && Object.keys(user).length > 0) {
                throw ("EMAIL_ALREADY_IN_USE");
            }
            //save user
            let userInfo = await this.authenticationHelper.saveUser(payload);
            console.log(">>>> userInfo >>>", JSON.stringify(userInfo));
            // parse for response
            let responseValue = this.objectParser.getUserResponseObj(userInfo);
            console.log(">>> response created >>>", responseValue);
            //create token 
            let token = await this.authenticationHelper.createJWT(responseValue);
            console.log(">>> token >>>", token);
            return Object.assign(Object.assign({}, responseValue), { token });
        }
        catch (err) {
            console.log("Caught in Err ####", err);
            throw err;
        }
    }
    /**
     * @desc Function to perform login
     * @param {{ email:string, password:string }} payload
     * @returns {userResponse}
     */
    async login(payload) {
        console.log("started with payload", payload);
        try {
            //get user 
            let user = await Users_1.default.findOne({
                email: payload.email,
                password: crypto.createHash('md5').update(payload.password).digest('hex'),
                deleted: 0
            });
            if (!user) {
                throw ("INVALID_EMAIL_OR_PASSWORD");
            }
            //parse to make repsonse obj
            let responseValue = this.objectParser.getUserResponseObj(user);
            console.log(">>> response created >>>", responseValue);
            //create token 
            let token = await this.authenticationHelper.createJWT(responseValue);
            console.log(">>> token >>>", token);
            return Object.assign(Object.assign({}, responseValue), { token });
        }
        catch (err) {
            console.log("Caught in Err ####", err);
            throw err;
        }
    }
    /**
     * @desc Function to create a host type user
     * @param {signUpRequest}} payload
     * @returns {userResponse}
     */
    async createHost(payload) {
        console.log("started with payload", payload);
        try {
            if (payload.type != "Host") {
                throw "User type is not correct";
            }
            //get user 
            let user = await Users_1.default.find({
                email: payload.email,
                deleted: 0
            });
            console.log("@@@@user", user);
            if (user && Object.keys(user).length > 0) {
                throw "EMAIL_ALREADY_IN_USE";
            }
            //save user
            let userInfo = await this.authenticationHelper.saveUser(payload);
            console.log(">>>> userInfo >>>", JSON.stringify(userInfo));
            // parse for response
            let responseValue = this.objectParser.getUserResponseObj(userInfo);
            console.log(">>> response created >>>", responseValue);
            // //create token 
            // let token = await this.authenticationHelper.createJWT(responseValue);
            // console.log(">>> token >>>",token);
            return Object.assign({}, responseValue);
        }
        catch (err) {
            console.log("Caught in Err ####", err);
            throw err;
        }
    }
    /**
     * @desc Function to delete host user
     * @param {{ hostId : string, superAdminId : string }} payload
     * @returns {boolean}
     */
    async deleteHost(payload) {
        console.log("Authentication.ts >>> started with payload", payload);
        try {
            //get user 
            let userInDB = await Users_1.default.findOne({
                _id: payload.hostId,
                type: "Host"
            });
            console.log("user in DB", userInDB);
            if (!userInDB) {
                console.log("user not exist");
                return false;
            }
            if (userInDB && userInDB.superAdminId != payload.superAdminId) {
                console.log("You are not authorized to perform this action");
                return false;
            }
            //get user 
            let user = await Users_1.default.updateOne({
                _id: payload.hostId,
                type: "Host",
                deleted: 0
            }, {
                deleted: 1
            });
            console.log("@@@@user", user);
            return true;
        }
        catch (err) {
            console.log("Caught in Err ####", err);
            throw err;
        }
    }
    /**
     * @desc Function to update host user
     * @param {{ fname? : string, lname? :string, email? :string, hostId : string, superAdminId : string}}} payload
     * @returns {boolean}
     */
    async updateHost(payload) {
        console.log("started with payload", payload);
        try {
            //get user 
            let userInDB = await Users_1.default.findOne({
                _id: payload.hostId,
                type: "Host"
            });
            console.log("user in DB", userInDB);
            if (!userInDB) {
                console.log("user not exist");
                return false;
            }
            if (userInDB && userInDB.superAdminId != payload.superAdminId) {
                throw ("You are not authorized to perform this action");
            }
            let input = {};
            if (payload.fname) {
                input.fname = payload.fname;
            }
            if (payload.lname) {
                input.lname = payload.lname;
            }
            if (payload.email) {
                input.email = payload.email;
            }
            console.log(input);
            //update user 
            let user = await Users_1.default.updateOne({
                _id: payload.hostId,
                type: "Host",
                deleted: 0
            }, input);
            console.log("@@@@user");
            return true;
        }
        catch (err) {
            console.log("Caught in Err ####", err);
            throw err;
        }
    }
    /**
    * @desc Function to create a guest type user
    * @param {signUpRequest}} payload
    * @returns {userResponse}
    */
    async createGuest(payload) {
        console.log("started with payload", payload);
        try {
            if (payload.type != "Guest") {
                throw "User type is not correct";
            }
            //get user 
            let user = await Users_1.default.find({
                email: payload.email,
                deleted: 0
            });
            console.log("@@@@user", user);
            if (user && Object.keys(user).length > 0) {
                throw ("EMAIL_ALREADY_IN_USE");
            }
            //save user
            let userInfo = await this.authenticationHelper.saveUser(payload);
            console.log(">>>> userInfo >>>", JSON.stringify(userInfo));
            // parse for response
            let responseValue = this.objectParser.getUserResponseObj(userInfo);
            console.log(">>> response created >>>", responseValue);
            // //create token 
            // let token = await this.authenticationHelper.createJWT(responseValue);
            // console.log(">>> token >>>",token);
            return Object.assign({}, responseValue);
        }
        catch (err) {
            console.log("Caught in Err ####", err);
            throw err;
        }
    }
    /**
     * @desc Function to delete Guest type user
     * @param {{ guestId : string , hostId? : string , superAdminId : string  }}} payload
     * @returns {boolean}
     */
    async deleteGuest(payload) {
        console.log("started with payload", payload);
        try {
            //get user 
            let userInDB = await Users_1.default.findOne({
                _id: payload.guestId,
                type: "Guest"
            });
            console.log("user in DB", userInDB);
            if (!userInDB) {
                console.log("user not exist");
                return false;
            }
            if (payload.hostId && payload.hostId != userInDB.hostId) {
                throw ("You are not authorized to perform this action");
            }
            if (payload.superAdminId != userInDB.superAdminId) {
                throw ("You are not authorized to perform this action");
            }
            //get user 
            let user = await Users_1.default.updateOne({
                _id: payload.guestId,
                type: "Guest",
                deleted: 0
            }, {
                deleted: 1
            });
            console.log("@@@@user", user);
            return true;
        }
        catch (err) {
            console.log("Caught in Err ####", err);
            throw err;
        }
    }
    /**
     * @desc Function to update guest user
     * @param {{ fname? : string,lname? :string,email? :string, guestId : string , hostId? : string , superAdminId : string}}} payload
     * @returns {boolean}
     */
    async updateGuest(payload) {
        console.log("started with payload", payload);
        try {
            //get user 
            let userInDB = await Users_1.default.findOne({
                _id: payload.guestId,
                type: "Guest"
            });
            console.log("user in DB", userInDB);
            if (!userInDB) {
                console.log("user not exist");
                return false;
            }
            if (payload.hostId && payload.hostId != userInDB.hostId) {
                throw ("You are not authorized to perform this action");
            }
            if (payload.superAdminId != userInDB.superAdminId) {
                throw ("You are not authorized to perform this action");
            }
            let input = {};
            if (payload.fname) {
                input.fname = payload.fname;
            }
            if (payload.lname) {
                input.lname = payload.lname;
            }
            if (payload.email) {
                input.email = payload.email;
            }
            console.log(input);
            //get user 
            let user = await Users_1.default.updateOne({
                _id: payload.guestId,
                type: "Guest",
                deleted: 0
            }, input);
            console.log("@@@@user", user);
            return true;
        }
        catch (err) {
            console.log("Caught in Err ####", err);
            throw err;
        }
    }
    /**
     * @desc Function to get all hosts for a super admin
     * @param {{ superAdminId : string }}} payload
     * @returns {userResponse}
     */
    async getHostsOfSuperAdmins(payload) {
        console.log("started with payload", payload);
        try {
            //get user 
            let users = await Users_1.default.find({
                superAdminId: payload.superAdminId,
                type: "Host",
                deleted: 0
            });
            console.log("@@@@user", users);
            if (users.length == 0) {
                return [];
            }
            return { users };
        }
        catch (err) {
            console.log("Caught in Err ####", err);
            throw err;
        }
    }
    /**
     * @desc Function to get all guests for a super admin
     * @param {{ superAdminId : string }}} payload
     * @returns {userResponse}
     */
    async getGuestsOfSuperAdmins(payload) {
        console.log("started with payload", payload);
        try {
            //get user 
            let users = await Users_1.default.find({
                superAdminId: payload.superAdminId,
                type: "Guest",
                deleted: 0
            });
            console.log("@@@@user", users);
            if (users.length == 0) {
                return [];
            }
            return { users };
        }
        catch (err) {
            console.log("Caught in Err ####", err);
            throw err;
        }
    }
}
exports.Authentication = Authentication;
//Test Script
//(()=>{
//     var test = new Authentication()
//     var input = {
//         fname : "testing",
//         email : "omiddddd@test.com",
//         hostId : "64fec5a8cfcc252d58c99eca",
//         superAdminId : "64fec586cfcc252d58c99ec7"
//     }
//     test.updateHost(input);
// })()
