import { userResponse, signUpRequest, createHost, createGuest } from "./../Datatypes/requestInterface";
import { Boolean } from "aws-sdk/clients/apigateway";
export declare class Authentication {
    private objectParser;
    private authenticationHelper;
    constructor();
    /**
     * @desc Function to create new Super Admin
     * @param {signUpRequest}} payload
     * @returns {userResponse}
     */
    signUp(payload: signUpRequest): Promise<userResponse>;
    /**
     * @desc Function to perform login
     * @param {{ email:string, password:string }} payload
     * @returns {userResponse}
     */
    login(payload: {
        email: string;
        password: string;
    }): Promise<userResponse>;
    /**
     * @desc Function to create a host type user
     * @param {signUpRequest}} payload
     * @returns {userResponse}
     */
    createHost(payload: createHost): Promise<userResponse>;
    /**
     * @desc Function to delete host user
     * @param {{ hostId : string, superAdminId : string }} payload
     * @returns {boolean}
     */
    deleteHost(payload: {
        hostId: string;
        superAdminId: string;
    }): Promise<Boolean>;
    /**
     * @desc Function to update host user
     * @param {{ fname? : string, lname? :string, email? :string, hostId : string, superAdminId : string}}} payload
     * @returns {boolean}
     */
    updateHost(payload: {
        fname?: string;
        lname?: string;
        email?: string;
        hostId: string;
        superAdminId: string;
    }): Promise<boolean>;
    /**
    * @desc Function to create a guest type user
    * @param {signUpRequest}} payload
    * @returns {userResponse}
    */
    createGuest(payload: createGuest): Promise<userResponse>;
    /**
     * @desc Function to delete Guest type user
     * @param {{ guestId : string , hostId? : string , superAdminId : string  }}} payload
     * @returns {boolean}
     */
    deleteGuest(payload: {
        guestId: string;
        hostId?: string;
        superAdminId: string;
    }): Promise<Boolean>;
    /**
     * @desc Function to update guest user
     * @param {{ fname? : string,lname? :string,email? :string, guestId : string , hostId? : string , superAdminId : string}}} payload
     * @returns {boolean}
     */
    updateGuest(payload: {
        fname?: string;
        lname?: string;
        email?: string;
        guestId: string;
        hostId?: string;
        superAdminId: string;
    }): Promise<boolean>;
    /**
     * @desc Function to get all hosts for a super admin
     * @param {{ superAdminId : string }}} payload
     * @returns {userResponse}
     */
    getHostsOfSuperAdmins(payload: {
        superAdminId: string;
    }): Promise<never[] | {
        users: any;
    }>;
    /**
     * @desc Function to get all guests for a super admin
     * @param {{ superAdminId : string }}} payload
     * @returns {userResponse}
     */
    getGuestsOfSuperAdmins(payload: {
        superAdminId: string;
    }): Promise<never[] | {
        users: any;
    }>;
}
