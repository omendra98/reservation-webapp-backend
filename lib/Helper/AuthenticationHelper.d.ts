import { userResponse, signUpRequest } from "./../Datatypes/requestInterface";
export declare class AuthenticationHelper {
    constructor();
    /**
     * @desc Function to create jwt token and return
     * @param {userResponse} user
     * @return { Promise<string> }
     */
    createJWT(user: userResponse): Promise<string>;
    /**
      * @desc Function to save user in db
      * @param {signUpRequest} payload
      * @return { userResponse }
      */
    saveUser(payload: signUpRequest): Promise<any>;
}
