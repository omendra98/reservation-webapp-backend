"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticationHelper = void 0;
//import constants
const config = require('../../config');
//import modules
const Users_1 = __importDefault(require("../Schemas/Users/Users"));
// @ts-ignore
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
var bcrypt = require('bcryptjs');
var crypto = require('crypto');
class AuthenticationHelper {
    constructor() {
    }
    /**
     * @desc Function to create jwt token and return
     * @param {userResponse} user
     * @return { Promise<string> }
     */
    async createJWT(user) {
        console.log("creating jwt token >>");
        try {
            let privateKey = config.JWT_SECRET;
            return jsonwebtoken_1.default.sign({
                id: user.id,
                fname: user.fname,
                lname: user.lname,
                email: user.email,
                type: user.type,
                superAdminId: user.superAdminId ? user.superAdminId : ''
            }, config.JWT_SECRET, {
                expiresIn: 86400, //expires in 24 hours
            });
        }
        catch (err) {
            throw err;
        }
    }
    /**
      * @desc Function to save user in db
      * @param {signUpRequest} payload
      * @return { userResponse }
      */
    async saveUser(payload) {
        console.log(">>> started with payload >>> ", payload);
        try {
            let dbObject = {
                fname: payload.fname,
                lname: payload.lname,
                email: payload.email,
                type: payload.type,
                password: crypto.createHash('md5').update(payload.password).digest('hex'),
                deleted: payload.deleted,
                superAdminId: payload.superAdminId ? payload.superAdminId : "",
                hostId: payload.hostId ? payload.hostId : "",
                guestId: payload.guestId ? payload.guestId : "",
            };
            console.log(">>going to save >>>", dbObject);
            let response = await await Users_1.default.create(dbObject);
            return response;
        }
        catch (err) {
            throw err;
        }
    }
}
exports.AuthenticationHelper = AuthenticationHelper;
