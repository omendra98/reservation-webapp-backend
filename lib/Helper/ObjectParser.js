"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ObjectParser = void 0;
class ObjectParser {
    getUserResponseObj(user) {
        try {
            let responseValue = {
                id: user._id,
                fname: user.fname,
                lname: user.lname,
                email: user.email,
                type: user.type,
                password: user.password,
                deleted: user.deleted,
                createdAt: user.createdAt,
                superAdminId: user.superAdminId ? user.superAdminId : "",
                hostId: user.hostId ? user.hostId : "",
                guestId: user.guestId ? user.guestId : ""
            };
            return responseValue;
        }
        catch (err) {
            throw err;
        }
    }
}
exports.ObjectParser = ObjectParser;
