export interface Reservation {
    superAdminId: string;
    propertyId: string;
    guestId: string;
    startDate: string;
    endDate: string;
    checkedIn?: boolean;
    checkedOut?: boolean;
    cancelled?: boolean;
    createdAt?: string;
}
