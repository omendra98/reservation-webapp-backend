// connect db on loading of package
// Importing required libraries/modules
const mongoose = require('mongoose');
const config = require('../../config');
/**
 * @desc Class to act as wrapper class for Db connectivity 
 * It gets initialized Environment name. 
 * After initializing,it will expose functions to create DB connection.
 * @class vkMongoDbConnection
 */
class vkMongoDbConnection {

    /**
     * Constructor will take env and load its config for further usage.
     * @param {*} env String 
     */
     constructor() {
        //if no env is provided throw err
        /* if (!env) {
            console.log(">>> vkMongoDbConnection -> constructor <<< Invalid environment provided for database connectivity");
            throwError(400,"Invalid environment provided for database connectivity");
        } */
        //if config of Env does not exist, throw err 
        if(config){
            this.config = config;
            console.log(">>> vkMongoDbConnection -> constructor <<< Environment loaded for Database Connectivity");
        }else{
            console.log(">>> vkMongoDbConnection -> constructor <<< Err in loading environment's configration for Database Connectivity");
            throwError(400,`No configration found for the enviorment`);
        }
    }
    /**
     * @desc Function to make a connection with Mongo Database based on Environment provided.
     *  After Successfully connectivity, it will return db object for further use if required.
     *  @returns Database connection Object
     */
    async connectDb() {
        try{

            console.log(`>>> vkMongoDbConnection -> connectDb <<< options for mongo connectivity `,this.config.DB_URL)

            //creating actual connection
            let connection = await mongoose
            .connect(this.config.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
            console.log(`>>> vkMongoDbConnection -> connectDb <<<  #### ${this.config.DB_NAME} ##### database connected successfully`)
       
            return connection;
        }catch(err){
            console.log("Err in Mongo Connectivity >> "+err)
            throwError(400,`Err in Mongo Connectivity`);
        }
    }
}

/**
 * @desc Funtion to throw err.
 */
    let throwError = (code,msg)=>{
        let err = new Error(msg);
        err.statusCode = code;
        throw err;
    }
const mongoDbObj = new vkMongoDbConnection();
mongoDbObj.connectDb();
module.exports = new vkMongoDbConnection();


