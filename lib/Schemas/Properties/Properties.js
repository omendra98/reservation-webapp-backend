"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const moment_timezone = require('moment-timezone');
const schema = new mongoose_1.Schema({
    superAdminId: {
        type: String,
    },
    name: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    timeZone: {
        type: String,
        required: true
    },
    isActive: {
        type: Boolean,
        default: true
    },
    isDisabled: {
        type: Boolean,
        default: true
    },
    createdAt: {
        type: String,
        default: (new Date()).toISOString()
    },
}, {
    timestamps: false,
});
exports.default = mongoose_1.models.Properties || (0, mongoose_1.model)('Properties', schema, 'Properties');
