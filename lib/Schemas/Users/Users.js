"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const moment_timezone = require('moment-timezone');
const schema = new mongoose_1.Schema({
    superAdminId: {
        type: String,
    },
    hostId: {
        type: String,
    },
    guestId: {
        type: String,
    },
    fname: {
        type: String,
        required: true
    },
    lname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    deleted: {
        type: Boolean,
        default: 0,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    createdAt: {
        type: Number,
        default: moment_timezone((new Date())).unix()
    },
}, {
    timestamps: false,
});
exports.default = mongoose_1.models.Users || (0, mongoose_1.model)('Users', schema, 'Users');
