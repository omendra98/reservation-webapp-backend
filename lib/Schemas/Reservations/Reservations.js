"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const moment_timezone = require('moment-timezone');
const schema = new mongoose_1.Schema({
    superAdminId: {
        type: String,
        required: true
    },
    propertyId: {
        type: String,
        required: true
    },
    guestId: {
        type: String,
        required: true
    },
    startDate: {
        type: Date,
        default: true
    },
    endDate: {
        type: Date,
        default: false
    },
    checkedIn: {
        type: Boolean,
        default: false
    },
    checkedOut: {
        type: Boolean,
        default: false
    },
    cancelled: {
        type: Boolean,
        default: false
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    createdAt: {
        type: String,
        default: (new Date()).toISOString()
    },
}, {
    timestamps: false,
});
exports.default = mongoose_1.models.Reservations || (0, mongoose_1.model)('Reservations', schema, 'Reservations');
