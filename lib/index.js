"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReservationClass = exports.PropertiesClass = exports.Authentication = void 0;
var Authentication_1 = require("./Classes/Authentication");
Object.defineProperty(exports, "Authentication", { enumerable: true, get: function () { return Authentication_1.Authentication; } });
var Properties_1 = require("./Classes/Properties");
Object.defineProperty(exports, "PropertiesClass", { enumerable: true, get: function () { return Properties_1.PropertiesClass; } });
var Reservations_1 = require("./Classes/Reservations");
Object.defineProperty(exports, "ReservationClass", { enumerable: true, get: function () { return Reservations_1.ReservationClass; } });
let schema = require("./Schemas/connectMongo");
__exportStar(require("./Datatypes/requestInterface"), exports);
__exportStar(require("./Datatypes/properties"), exports);
__exportStar(require("./Datatypes/reservations"), exports);
