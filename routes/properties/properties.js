const express = require('express');
const { body } = require('express-validator');
const router = express.Router();

const propertiesMiddlewares = require('../../controllers/properties/properties');
const authMiddleware = require('../../controllers/auth/auth-middleware');
const validateUser = require('../../controllers/auth/validateUser.js');


router.post(
    '/createProperty',
    authMiddleware(['SuperAdmin','Host']),
    validateUser(),
    [
        body('name').exists().isLength({ min: 4 }),
        body('address').trim().isLength({ min: 4 }),
        body('timeZone').trim().isLength({ min: 2 }),
        body('superAdminId').trim().isLength({min : 12}),
    ],
    propertiesMiddlewares.createProperty
);


router.post(
    '/getProperties',
    authMiddleware(['SuperAdmin','Host']),
    validateUser(),
    [
        body('superAdminId').trim().isLength({min : 12}),
    ],
    propertiesMiddlewares.getProperties
);


router.post(
    '/getProperty',
    authMiddleware(['SuperAdmin','Host']),
    validateUser(),
    [
        body('superAdminId').trim().isLength({min : 12}),
        body('_id').trim().isLength({min : 12}),
    ],
    propertiesMiddlewares.getProperty
);


router.post(
    '/updateProperty',
    authMiddleware(['SuperAdmin','Host']),
    validateUser(),
    [
        body('superAdminId').trim().isLength({min : 12}),
        body('propertyId').trim().isLength({min : 12}),
    ],
    propertiesMiddlewares.updateProperty
);


router.post(
    '/deleteProperty',
    authMiddleware(['SuperAdmin','Host']),
    validateUser(),
    [
        body('superAdminId').trim().isLength({min : 12}),
        body('propertyId').trim().isLength({min : 12}),
    ],
    propertiesMiddlewares.deleteProperty
);

module.exports = router;
