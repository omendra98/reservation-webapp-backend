const express = require('express');
const { body } = require('express-validator');
const router = express.Router();

const brandAuthMiddlewares = require('../../controllers/company/auth');
const authMiddleware = require('../../controllers/auth/auth-middleware');
const validateUser = require('../../controllers/auth/validateUser.js');

router.post(
    '/register',
    [
        body('email').isEmail().normalizeEmail().isLength({ min: 8 }),
        body('password').trim(),
        body('fname').trim(),
        body('lname').trim(),
        body('type').trim(),
        body('deleted').trim()
    ],
    brandAuthMiddlewares.createUser
);

router.post('/login', [body('email').isEmail(), body('password').trim().isLength({ min: 6 })], brandAuthMiddlewares.login);


router.post(
    '/createHost',
    authMiddleware(['SuperAdmin']),
    validateUser(),
    [
        body('email').isEmail().isLength({ min: 8 }),
        body('password').trim(),
        body('fname').trim(),
        body('lname').trim(),
        body('type').trim().exists(),
        body('deleted').trim(),
        body('superAdminId').trim().isLength({min : 12})
    ],
    brandAuthMiddlewares.createHost
);

router.post(
    '/deleteHost',
    authMiddleware(['SuperAdmin']),
    validateUser(),
    [
        body('hostId').trim().isLength({min : 12}),
        body('superAdminId').trim().isLength({min : 12})
    ],
    brandAuthMiddlewares.deleteHost
);

router.post(
    '/updateHost',
    authMiddleware(['SuperAdmin']),
    validateUser(),
    [
        body('superAdminId').trim().isLength({min : 12}),
        body('hostId').trim().isLength({min : 12})
    ],
    brandAuthMiddlewares.updateHost
);

router.post(
    '/updateGuest',
    authMiddleware(['SuperAdmin','Host']),
    validateUser(),
    [
        body('superAdminId').trim().isLength({min : 12}),
        body('guestId').trim().isLength({min : 12})
    ],
    brandAuthMiddlewares.updateGuest
);


router.post(
    '/createGuest',
    authMiddleware(['SuperAdmin','Host']),
    validateUser(),
    [
        body('email').isEmail().isLength({ min: 8 }),
        body('password').trim(),
        body('fname').trim(),
        body('lname').trim(),
        body('type').trim(),
        body('deleted').trim(),
        body('superAdminId').trim().isLength({min : 12}),
    ],
    brandAuthMiddlewares.createGuest
);

router.post(
    '/deleteGuest',
    authMiddleware(['SuperAdmin','Host']),
    validateUser(),
    [
        body('superAdminId').trim(),
        body('guestId').trim().isLength({min : 12}),
    ],
    brandAuthMiddlewares.deleteGuest
);


router.post(
    '/getHostsOfSuperAdmins',
    authMiddleware('SuperAdmin'),
    validateUser(),
    [
        body('superAdminId').trim()
    ],
    brandAuthMiddlewares.getHostsOfSuperAdmins
);

router.post(
    '/getGuestsOfSuperAdmins',
    authMiddleware('SuperAdmin'),
    validateUser(),
    [
        body('superAdminId').trim()
    ],
    brandAuthMiddlewares.getGuestsOfSuperAdmins
);



module.exports = router;
