const express = require('express');
const { body } = require('express-validator');
const router = express.Router();

const staysMiddlewares = require('../../controllers/reservations/reservations');
const authMiddleware = require('../../controllers/auth/auth-middleware');
const validateUser = require('../../controllers/auth/validateUser.js');


router.post(
    '/addStay',
    authMiddleware(['SuperAdmin','Host']),
    validateUser(),
    [
        body('superAdminId').trim().isLength({min : 12}),
        body('propertyId').trim().isLength({min : 12}),
        body('guestId').trim().isLength({min : 12}),
        body('startDate').trim(),
        body('endDate').trim(),
    ],
    staysMiddlewares.addStay
);


router.post(
    '/updateStayByHosts',
    authMiddleware(['SuperAdmin','Host']),
    validateUser(),
    [
        body('superAdminId').trim().isLength({min : 12}),
        body('reservationId').trim().isLength({min : 12}),
        body('cancelled').exists()
    ],
    staysMiddlewares.updateStayByHosts
);


router.post(
    '/updateStayByGuest',
    authMiddleware(['Guest']),
    validateUser(),
    [
        body('superAdminId').trim().isLength({min : 12}),
        body('reservationId').trim().isLength({min : 12}),
    ],
    staysMiddlewares.updateStayByGuest
);


router.post(
    '/getReservationsByProperty',
    authMiddleware(['SuperAdmin','Host']),
    validateUser(),
    [
        body('superAdminId').trim().isLength({min : 12}),
        body('propertyId').trim().isLength({min : 12}),
    ],
    staysMiddlewares.getReservationsByProperty
);


router.post(
    '/getCurrentReservationsByProperty',
    authMiddleware(['SuperAdmin','Host']),
    validateUser(),
    [
        body('superAdminId').trim().isLength({min : 12}),
        body('propertyId').trim().isLength({min : 12}),
    ],
    staysMiddlewares.getCurrentReservationsByProperty
);

router.post(
    '/getUpcomingReservationsByProperty',
    authMiddleware(['SuperAdmin','Host']),
    validateUser(),
    [
        body('superAdminId').trim().isLength({min : 12}),
        body('propertyId').trim().isLength({min : 12}),
    ],
    staysMiddlewares.getUpcomingReservationsByProperty
);

router.post(
    '/getReservationsForGuest',
    authMiddleware(['Guest']),
    validateUser(),
    [
        body('superAdminId').trim().isLength({min : 12}),
        body('guestId').trim().isLength({min : 12}),
    ],
    staysMiddlewares.getReservationsForGuest
);

router.post(
    '/getAllReservations',
    authMiddleware(['SuperAdmin','Host']),
    validateUser(),
    [
        body('superAdminId').trim().isLength({min : 12}),
    ],
    staysMiddlewares.getAllReservations
);

module.exports = router;
