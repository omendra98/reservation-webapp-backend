const { validationResult } = require('express-validator');
var jwt = require('jsonwebtoken');

var config = require('../../config');

const { PropertiesClass } = require("../../lib/index"),
PropertiesCore = new PropertiesClass();

let middlewares = {};
// function to get hosts for super admin
const createProperty = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.body);
		let result =  await PropertiesCore.createProperty(req.body);
        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
        res.status(400).send({ error: true, message: err })
	}
};


const getProperties = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.body);
		let result =  await PropertiesCore.getProperties(req.body);
        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
        res.status(400).send({ error: true, message: err })
	}
};


const getProperty = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.body);
		let result =  await PropertiesCore.getProperty(req.body);
        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
        res.status(400).send({ error: true, message: err })
	}
};

const updateProperty = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.body);
		let result =  await PropertiesCore.updateProperty(req.body);
        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
        res.status(400).send({ error: true, message: err })
	}
};


const deleteProperty = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.body);
		let result =  await PropertiesCore.deleteProperty(req.body);
        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
        res.status(400).send({ error: true, message: err })
	}
};

middlewares = {
    createProperty,
    getProperties,
    getProperty,
    updateProperty,
    deleteProperty
}

module.exports = middlewares;
