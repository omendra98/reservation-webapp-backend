const { validationResult } = require('express-validator');
var jwt = require('jsonwebtoken');

var config = require('../../config');

const { ReservationClass } = require("../../lib/index"),
ReservationCore = new ReservationClass();

let middlewares = {};
// function to get hosts for super admin
const addStay = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.body);
		let result =  await ReservationCore.addStay(req.body);
        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
        res.status(400).send({ error: true, message: err })
	}
};


const updateStayByHosts = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.body);
		let result =  await ReservationCore.updateStayByHosts(req.body);
        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
        res.status(400).send({ error: true, message: err })
	}
};


const updateStayByGuest = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.body);
		let result =  await ReservationCore.updateStayByGuest(req.body);
        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
        res.status(400).send({ error: true, message: err })
	}
};

const getReservationsByProperty = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.body);
		let result =  await ReservationCore.getReservationsByProperty(req.body);
        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
        res.status(400).send({ error: true, message: err })
	}
};


const getCurrentReservationsByProperty = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.body);
		let result =  await ReservationCore.getCurrentReservationsByProperty(req.body);
        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
        res.status(400).send({ error: true, message: err })
	}
};

const getUpcomingReservationsByProperty = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.body);
		let result =  await ReservationCore.getUpcomingReservationsByProperty(req.body);
        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
        res.status(400).send({ error: true, message: err })
	}
};


const getReservationsForGuest = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.body);
		let result =  await ReservationCore.getReservationsForGuest(req.body);
        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
        res.status(400).send({ error: true, message: err })
	}
};

const getAllReservations = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.body);
		let result =  await ReservationCore.getAllReservations(req.body);
        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
        res.status(400).send({ error: true, message: err })
	}
};

middlewares = {
    addStay,
    updateStayByHosts,
    updateStayByGuest,
    getReservationsByProperty,
    getCurrentReservationsByProperty,
    getUpcomingReservationsByProperty,
    getReservationsForGuest,
    getAllReservations
}

module.exports = middlewares;
