const { validationResult } = require('express-validator');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var crypto = require('crypto');

var config = require('../../config');

const { Authentication } = require("../../lib/index"),
authenticationCore = new Authentication();

let middlewares = {};

// function to create a new super admin user
const createUser = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.body);
		let result =  await authenticationCore.signUp(req.body);
        console.log(">>",result)
        res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
		console.log("Caught in Err ######", err);
        res.status(400).send({ error: true, message: err })
		throw (err);
	}
};



// function to login a user
const login = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.payload);
		let result =  await authenticationCore.login(req.body);

        res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
		console.log("Caught in Err ######", err);
        res.status(400).send({ error: true, message: err })
	}
};

// function to get hosts for super admin
const createHost = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.payload);
		let result =  await authenticationCore.createHost(req.body);
        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
        res.status(400).json({ error: true, message: err });
	}
};

// function to get hosts for super admin
const deleteHost = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		let result =  await authenticationCore.deleteHost(req.body);
        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
        res.status(400).json({ error: true, message: err });
	}
};


// function to get guests for super admin
const updateHost = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.payload);
		let result =  await authenticationCore.updateHost(req.body);

        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
		res.status(400).json({ error: true, message: err });
	}
};


// function to get hosts for super admin
const createGuest = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.payload);
		let result =  await authenticationCore.createGuest(req.body);

        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
		console.log("Caught in Err ######", err);
        res.status(400).json({ error: true, message: err });
	}
};

// function to get guests for super admin
const deleteGuest = async (req, res) => {
    
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.payload);
		let result =  await authenticationCore.deleteGuest(req.body);

        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
		console.log("Caught in Err ######", err);
        res.status(400).json({ error: true, message: err });
	}
};

// function to get guests for super admin
const updateGuest = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		console.log("started with payload", req.payload);
		let result =  await authenticationCore.updateGuest(req.body);

        return res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
		console.log("Caught in Err ######", err);
        res.status(400).json({ error: true, message: err });
	}
};

// function to get hosts for super admin
const getHostsOfSuperAdmins = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		let result =  await authenticationCore.getHostsOfSuperAdmins(req.body);
        res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
		console.log("Caught in Err ######", err);
        res.status(400).json({ error: true, message: err });
		throw (err);
	}
};


// function to get hosts for super admin
const getGuestsOfSuperAdmins = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ error: true, message: 'Validation errors in user fields.' });
    }
    try {
		let result =  await authenticationCore.getGuestsOfSuperAdmins(req.body);
        res.status(200).send({ error: false, result: { result : result } });
	} catch (err) {
		console.log("Caught in Err ######", err);
        res.status(400).json({ error: true, message: err });
		throw (err);
	}
};


middlewares = {
    login,
    createUser,
    createHost,
    deleteHost,
    createGuest,
    deleteGuest,
    updateHost,
    updateGuest,
    getHostsOfSuperAdmins,
    getGuestsOfSuperAdmins
}

module.exports = middlewares;
