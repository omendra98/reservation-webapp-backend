const jwt = require('jsonwebtoken');

const config = require('../../config');

const authMiddleware = (userType) => {
    return (req, res, next) => {
        jwt.verify(req.headers['authorization'], config.JWT_SECRET, (error, decoded_token) => {
            console.log("Decoded Token>>> ",decoded_token);
            if (error) {
                res.status(401).json({ error: true, message: [error.message] });
            } else {
                if (userType && !userType.includes(decoded_token.type)) {
                    res.status(401).json({ error: true, message: 'User not authorized' });
                } else {
                    req.type = decoded_token.type;
                    req.id = decoded_token.id;
                    req.superAdminId= decoded_token.superAdminId;
                    next();
                }
            }
        });
    };
};


module.exports = authMiddleware;
