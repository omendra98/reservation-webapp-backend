const jwt = require('jsonwebtoken');

const config = require('../../config');

const validateUser = () => {
    return (req, res, next) => {
            if (!req.id) {
                res.status(401).json({ error: true });
            }
            const arr = [req.body.superAdminId]
            if (req.type=="SuperAdmin" && !arr.includes(req.id)) {
                    console.log("Invalid Super Admin.");
                    return res.status(401).json({ error: true, message: 'User not authorized' });
            }
            if (req.type=="Host" && req.body.superAdminId != req.superAdminId) {
                console.log("Invalid Host");
                return res.status(401).json({ error: true, message: 'User not authorized' });
            }
            if (req.type=="Guest" && req.body.superAdminId != req.superAdminId) {
                console.log("Invalid Guest");
                return res.status(401).json({ error: true, message: 'User not authorized' });
            }
            console.log(req.id);
            console.log("User is validated.");
            next();
    };
};

module.exports = validateUser;
