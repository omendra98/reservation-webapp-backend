const express = require('express');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');

const config = require('./config');


const companyAuthRoutes = require('./routes/company/auth');
const propertyRoutes = require('./routes/properties/properties');
const stayRoutes = require('./routes/reservations/reservations');

const app = express();

// view engine setup
app.set('views', './views');
app.set('view engine', 'ejs');

// Use JSON body parser
app.use(bodyParser.json());

app.use(express.static(__dirname + '/views'));

// for parsing application/xwww-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// Enable CORS requests
app.use(cors());

// Use HTTP request console logging
app.use(morgan('combined'));

// Connect to MongoDB Atlas instance


//Users
app.use('/company', companyAuthRoutes);

//Properties
app.use('/property', propertyRoutes);

//Reservations
app.use('/stays', stayRoutes);


// Configure Express app
const appConfig = app.listen(config.PORT, config.HOST, () => {
    console.log(`Listening on ${appConfig.address().address}:${appConfig.address().port}`);
});
