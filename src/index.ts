export {Authentication} from "./Classes/Authentication";
export {PropertiesClass} from "./Classes/Properties";
export {ReservationClass} from "./Classes/Reservations";

let schema  = require("./Schemas/connectMongo");

export * from "./Datatypes/requestInterface";
export * from "./Datatypes/properties";
export * from "./Datatypes/reservations";
