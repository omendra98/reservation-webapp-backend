//import constants
//import third party modules
//import crypto from 'crypto';

//import modules

import Users from "../Schemas/Users/Users";
import {ObjectParser} from "./../Helper/ObjectParser";
let schema  = require("../Schemas/connectMongo.js")
var bcrypt = require('bcryptjs');
var crypto = require('crypto');

//local modules
import {AuthenticationHelper} from "./../Helper/AuthenticationHelper";

//interfaces
import {userResponse,signUpRequest,createHost,createGuest} from "./../Datatypes/requestInterface";
import { Boolean } from "aws-sdk/clients/apigateway";


export class Authentication{
    private objectParser:ObjectParser;
    private authenticationHelper:AuthenticationHelper;

    constructor(){
        this.objectParser = new ObjectParser();
        this.authenticationHelper = new AuthenticationHelper();
    }


    /**
     * @desc Function to create new Super Admin 
     * @param {signUpRequest}} payload
     * @returns {userResponse}
     */
    async signUp( payload:signUpRequest ) : Promise<userResponse> {
        console.log("started with payload",payload);

        try{
            if(payload.type != "SuperAdmin"){
                throw "User type is not correct"
            }
            //get user 
            let user:any = await Users.find({
                email :payload.email,
                deleted:0
            })
            console.log("@@@@user",user)
            if(user && Object.keys(user).length > 0 ) {
                throw ("EMAIL_ALREADY_IN_USE");
            }
            //save user
            let userInfo = await this.authenticationHelper.saveUser(payload);
            console.log(">>>> userInfo >>>",JSON.stringify(userInfo));

            // parse for response
            let responseValue =  this.objectParser.getUserResponseObj(userInfo)
            console.log(">>> response created >>>",responseValue);
            
            //create token 
            let token = await this.authenticationHelper.createJWT(responseValue);
            console.log(">>> token >>>",token);
            return {...responseValue,token};
            
        }catch(err){
            console.log("Caught in Err ####",err);
            throw err;
        }
        
    }   

    /**
     * @desc Function to perform login 
     * @param {{ email:string, password:string }} payload
     * @returns {userResponse}
     */
    async login( payload:{ email:string, password:string} ) : Promise<userResponse> {
        console.log("started with payload",payload);

        try{
            
            //get user 
            let user:any = await Users.findOne({
                email : payload.email,
                password: crypto.createHash('md5').update(payload.password).digest('hex'),
                deleted:0
            })
            if(!user){
                throw ("INVALID_EMAIL_OR_PASSWORD");
            }

            //parse to make repsonse obj
            let responseValue =  this.objectParser.getUserResponseObj(user)
            console.log(">>> response created >>>",responseValue);
            
            //create token 
            let token = await this.authenticationHelper.createJWT(responseValue);
            console.log(">>> token >>>",token);
            return {...responseValue , token};
            
        }catch(err){
            console.log("Caught in Err ####",err);
            throw err;
        }
    }


    /**
     * @desc Function to create a host type user
     * @param {signUpRequest}} payload
     * @returns {userResponse}
     */
    async createHost( payload:createHost ) : Promise<userResponse> {
        console.log("started with payload",payload);
        try{

            if(payload.type != "Host"){
                throw "User type is not correct"
            }
            //get user 
            let user:any = await Users.find({
                email : payload.email,
                deleted:0
            })
            console.log("@@@@user",user)
            if(user && Object.keys(user).length > 0 ) {
                throw "EMAIL_ALREADY_IN_USE";
            }
            //save user
            let userInfo = await this.authenticationHelper.saveUser(payload);
            console.log(">>>> userInfo >>>",JSON.stringify(userInfo));

            // parse for response
            let responseValue =  this.objectParser.getUserResponseObj(userInfo)
            console.log(">>> response created >>>",responseValue);
            
            // //create token 
            // let token = await this.authenticationHelper.createJWT(responseValue);
            // console.log(">>> token >>>",token);
            return {...responseValue};
            
        }catch(err){
            console.log("Caught in Err ####",err);
            throw err;
        }
        
    }


    /**
     * @desc Function to delete host user
     * @param {{ hostId : string, superAdminId : string }} payload
     * @returns {boolean}
     */
    async deleteHost( payload: { hostId : string, superAdminId : string } ) : Promise<Boolean> {
        console.log("Authentication.ts >>> started with payload",payload);
        try{
            //get user 
            let userInDB:any = await Users.findOne({
                _id : payload.hostId,
                type : "Host"
            })
            console.log("user in DB" , userInDB)
            if(!userInDB){
                console.log("user not exist");
                return false;
            }

            if(userInDB && userInDB.superAdminId != payload.superAdminId){
                console.log("You are not authorized to perform this action");
                return false;
            }

            //get user 
            let user:any = await Users.updateOne({
                _id : payload.hostId,
                type :"Host",
                deleted : 0               
            },{
                deleted : 1
            })
            console.log("@@@@user",user)

            return true;
        }catch(err){
            console.log("Caught in Err ####",err);
            throw err;
        }
        
    }


    /**
     * @desc Function to update host user
     * @param {{ fname? : string, lname? :string, email? :string, hostId : string, superAdminId : string}}} payload
     * @returns {boolean}
     */
    async updateHost( payload: { fname? : string,lname? :string,email? :string, hostId : string, superAdminId : string} ){
        console.log("started with payload",payload);
        try{
            //get user 
            let userInDB:any = await Users.findOne({
                _id : payload.hostId,
                type : "Host"
            })
            console.log("user in DB" , userInDB)
            if(!userInDB){
                console.log("user not exist");
                return false;
            }


            if(userInDB && userInDB.superAdminId != payload.superAdminId){
                throw ("You are not authorized to perform this action");
            }

            let input : any = {};
            if(payload.fname){
                input.fname = payload.fname;
            }
            if(payload.lname){
                input.lname = payload.lname;
            }
            if(payload.email){
                input.email = payload.email;
            }
            console.log(input)
            //update user 
            let user:any = await Users.updateOne({
                _id : payload.hostId,
                type :"Host",
                deleted : 0               
            },
            input
            )
            console.log("@@@@user")

            return true;
        }catch(err){
            console.log("Caught in Err ####",err);
            throw err;
        }
        
    }

     /**
     * @desc Function to create a guest type user
     * @param {signUpRequest}} payload
     * @returns {userResponse}
     */
     async createGuest( payload: createGuest ) : Promise<userResponse> {
        console.log("started with payload",payload);
        try{

            if(payload.type != "Guest"){
                throw "User type is not correct"
            }
            //get user 
            let user:any = await Users.find({
                email :payload.email,
                deleted:0
            })
            console.log("@@@@user",user)
            if(user && Object.keys(user).length > 0 ) {
                throw ("EMAIL_ALREADY_IN_USE");
            }
            //save user
            let userInfo = await this.authenticationHelper.saveUser(payload);
            console.log(">>>> userInfo >>>",JSON.stringify(userInfo));

            // parse for response
            let responseValue =  this.objectParser.getUserResponseObj(userInfo)
            console.log(">>> response created >>>",responseValue);
            
            // //create token 
            // let token = await this.authenticationHelper.createJWT(responseValue);
            // console.log(">>> token >>>",token);
            return {...responseValue};
            
        }catch(err){
            console.log("Caught in Err ####",err);
            throw err;
        }
        
    }


    /**
     * @desc Function to delete Guest type user
     * @param {{ guestId : string , hostId? : string , superAdminId : string  }}} payload
     * @returns {boolean}
     */
    async deleteGuest( payload: { guestId : string , hostId? : string , superAdminId : string  } ) : Promise<Boolean> {
        console.log("started with payload",payload);
        try{
            //get user 
            let userInDB:any = await Users.findOne({
                _id : payload.guestId,
                type : "Guest"
            })
            console.log("user in DB" , userInDB)
            if(!userInDB){
                console.log("user not exist");
                return false;
            }

            if(payload.hostId && payload.hostId != userInDB.hostId){
                throw ("You are not authorized to perform this action");
            }

            if(payload.superAdminId != userInDB.superAdminId){
                throw ("You are not authorized to perform this action");
            }
            //get user 
            let user:any = await Users.updateOne({
                _id : payload.guestId,
                type :"Guest",
                deleted : 0               
            },{
                deleted : 1
            })
            console.log("@@@@user",user)

            return true;
        }catch(err){
            console.log("Caught in Err ####",err);
            throw err;
        }
        
    }


    /**
     * @desc Function to update guest user
     * @param {{ fname? : string,lname? :string,email? :string, guestId : string , hostId? : string , superAdminId : string}}} payload
     * @returns {boolean}
     */
    async updateGuest( payload: { fname? : string,lname? :string,email? :string, guestId : string , hostId? : string , superAdminId : string} ){
        console.log("started with payload",payload);
        try{
            //get user 
            let userInDB:any = await Users.findOne({
                _id : payload.guestId,
                type : "Guest"
            })
            console.log("user in DB" , userInDB)
            if(!userInDB){
                console.log("user not exist");
                return false;
            }

            if(payload.hostId && payload.hostId != userInDB.hostId){
                throw ("You are not authorized to perform this action");
            }

            if(payload.superAdminId != userInDB.superAdminId){
                throw ("You are not authorized to perform this action");
            }

            let input : any = {};
            if(payload.fname){
                input.fname = payload.fname;
            }
            if(payload.lname){
                input.lname = payload.lname;
            }
            if(payload.email){
                input.email = payload.email;
            }
            console.log(input)
            //get user 
            let user:any = await Users.updateOne({
                _id : payload.guestId,
                type :"Guest",
                deleted : 0               
            },
            input
            )
            console.log("@@@@user",user)

            return true;
        }catch(err){
            console.log("Caught in Err ####",err);
            throw err;
        }
        
    }

    /**
     * @desc Function to get all hosts for a super admin
     * @param {{ superAdminId : string }}} payload
     * @returns {userResponse}
     */
        async getHostsOfSuperAdmins( payload: { superAdminId : string } ) {
            console.log("started with payload",payload);
            try{
    
                //get user 
                let users : any = await Users.find({
                    superAdminId : payload.superAdminId,
                    type : "Host",
                    deleted : 0
                })
                console.log("@@@@user",users)
                if(users.length ==0) {
                    return [];
                }
                
                return {users};
                
            }catch(err){
                console.log("Caught in Err ####",err);
                throw err;
            }
            
        }


    /**
     * @desc Function to get all guests for a super admin
     * @param {{ superAdminId : string }}} payload
     * @returns {userResponse}
     */
        async getGuestsOfSuperAdmins( payload: { superAdminId : string } ) {
            console.log("started with payload",payload);
            try{
    
                //get user 
                let users : any = await Users.find({
                    superAdminId : payload.superAdminId,
                    type : "Guest",
                    deleted : 0
                })
                console.log("@@@@user",users)
                if(users.length ==0) {
                    return [];
                }
                return {users};
                
            }catch(err){
                console.log("Caught in Err ####",err);
                throw err;
            }
            
        }

}


//Test Script
//(()=>{
//     var test = new Authentication()
//     var input = {
//         fname : "testing",
//         email : "omiddddd@test.com",
//         hostId : "64fec5a8cfcc252d58c99eca",
//         superAdminId : "64fec586cfcc252d58c99ec7"
//     }
//     test.updateHost(input);
// })()