import { Schema, model, models } from "mongoose";
const moment_timezone = require('moment-timezone');

const schema = new Schema({
    superAdminId: {
        type: String,
    },
    name: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    timeZone: {
        type: String,
        required: true
    },
    isActive: {
        type: Boolean,
        default: true
    },
    isDisabled : {
        type: Boolean,
        default : true
    },
    createdAt: {
        type: String,
        default: (new Date()).toISOString()
    },
}, {
    timestamps: false,
});

export default models.Properties || model('Properties', schema, 'Properties');