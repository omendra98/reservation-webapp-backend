import { Schema, model, models } from "mongoose";
const moment_timezone = require('moment-timezone');

const schema = new Schema({
    superAdminId: {
        type: String,
        required: true
    },
    propertyId: {
        type: String,
        required: true
    },
    guestId: {
        type: String,
        required: true
    },
    startDate: {
        type: Date,
        default: true
    },
    endDate : {
        type: Date,
        default : false
    },
    checkedIn: {
        type: Boolean,
        default: false
    },
    checkedOut : {
        type: Boolean,
        default : false
    },
    cancelled : {
        type: Boolean,
        default : false
    },
    isDeleted :{
        type: Boolean,
        default : false
    },
    createdAt: {
        type: String,
        default: (new Date()).toISOString()
    },
}, {
    timestamps: false,
});

export default models.Reservations || model('Reservations', schema, 'Reservations');