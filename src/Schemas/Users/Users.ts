import { Schema, model, models } from "mongoose";
const moment_timezone = require('moment-timezone');

const schema = new Schema({
    superAdminId: {
        type: String,
    },
    hostId: {
        type: String,
    },
    guestId: {
        type: String,
    },
    fname: {
        type: String,
        required: true
    },
    lname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    deleted: {
        type: Boolean,
        default : 0,
        required: true
    },
    type : {
        type: String,
        required: true
    },
    createdAt: {
        type: Number,
        default: moment_timezone((new Date())).unix()
    },
}, {
    timestamps: false,
});

export default models.Users || model('Users', schema, 'Users');