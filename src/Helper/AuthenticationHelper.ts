//import constants
const config = require('../../config');

//interfaces
import { userResponse,signUpRequest } from "./../Datatypes/requestInterface"

//import modules
import Users from "../Schemas/Users/Users";

// @ts-ignore
import jwt from "jsonwebtoken";
var bcrypt = require('bcryptjs');
var crypto = require('crypto');

export class AuthenticationHelper {
    constructor() {

    }

    /**
     * @desc Function to create jwt token and return
     * @param {userResponse} user 
     * @return { Promise<string> }
     */
    public async createJWT(user: userResponse): Promise<string> {
        console.log("creating jwt token >>");
        try {
            let privateKey =  config.JWT_SECRET;


            return jwt.sign({
                id: user.id,
                fname: user.fname,
                lname: user.lname,
                email: user.email,
                type: user.type,
                superAdminId : user.superAdminId? user.superAdminId : ''
            }, 
            config.JWT_SECRET,
            {
                expiresIn: 86400, //expires in 24 hours
            })

        } catch (err) {
            throw err;
        }

    }


   /**
     * @desc Function to save user in db
     * @param {signUpRequest} payload 
     * @return { userResponse }
     */

    async saveUser(payload:signUpRequest){
        console.log(">>> started with payload >>> ",payload)
        try{
            let dbObject:any = {
                fname           :   payload.fname,
                lname           :   payload.lname,
                email           :   payload.email,
                type            :   payload.type,
                password        :   crypto.createHash('md5').update(payload.password).digest('hex'),
                deleted         :   payload.deleted,
                superAdminId    :   payload.superAdminId ?  payload.superAdminId : "",
                hostId          :   payload.hostId ?  payload.hostId : "",
                guestId         :   payload.guestId ?  payload.guestId : "",
            }
            console.log(">>going to save >>>",dbObject)
            let response =  await await Users.create(dbObject);
            return response;
        }catch(err){
            throw err;
        }
    }

}
