import {userResponse} from "./../Datatypes/requestInterface";
export class ObjectParser{

    getUserResponseObj(user:any):userResponse{
        try{
            let responseValue:userResponse = {
                id: user._id,
                fname: user.fname,
                lname: user.lname,
                email: user.email,
                type: user.type,
                password : user.password,
                deleted : user.deleted,
                createdAt : user.createdAt,
                superAdminId    :   user.superAdminId ?  user.superAdminId : "",
                hostId          :   user.hostId ?  user.hostId : "",
                guestId         :   user.guestId ?  user.guestId : ""
                }
            return responseValue;
    
        }catch(err){
            throw err;
        }
    }
    
}