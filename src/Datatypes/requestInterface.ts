export interface userResponse{
    id:string,
    fname:string,
    lname:string,
    email:string,
    type:string,
    deleted:boolean,
    password : string,
    createdAt : number,
    token? : string,
    superAdminId? : string,
    hostId? : string,
    guestId? : string
}

export interface signUpRequest{
    fname:string,
    lname:string,
    email:string,
    type:string,
    password : string
    deleted :number,
    superAdminId? : string
    hostId? : string,
    guestId? : string
}

export interface createHost{
    fname:string,
    lname:string,
    email:string,
    type: string,
    password : string
    deleted :number,
    superAdminId : string
    hostId : string,
}

export interface createGuest{
    fname:string,
    lname:string,
    email:string,
    type: string,
    password : string
    deleted :number,
    superAdminId : string
    hostId : string,
    guestId? : string
}