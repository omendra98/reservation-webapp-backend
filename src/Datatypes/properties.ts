export interface Property { 
    superAdminId: string,
    _id : string,
    name: string, 
    address : string,
    timeZone : string,
    isActive? : boolean,
    isDisabled? : boolean,
    createdAt? : string
}

