# Reservation Webapp Backend

To start server, please follow the below steps:

### Step1 :
Clone the repository to your local machine using the below command
```
git clone https://gitlab.com/omendra98/reservation-webapp-backend.git
```

### Step2 :
Run below command to install the dependencies
```
npm install
```

### Step3 :
Run below command to start the server
```
npm start
```

## After the successfull start, server will be running at : http://127.0.0.1:4000/
